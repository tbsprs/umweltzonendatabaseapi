/**
 * Module dependencies.
 * express
 * pg
 */

var express = require('express');
var pg = require('pg');


var app = express();

// Returns the server state.
app.get('/', function(req, response) {
    response.write("Server is running normally.")
    response.end();
});

// Returns Umweltzonen changed before the annoucement of the week task
app.get('/before', function(req, response) {
    retrieveZones(response, 'test.umweltzonen_before');
});

// Returns Umweltzonen changed since the annoucement of the week task
app.get('/changed', function(req, response) {
    retrieveZones(response, 'test.umweltzonen_changed');
});


function retrieveZones(response, tableName) {

    var conString = "postgres://<USER>:<PASSWORD>@localhost/<DATABASE>";

    var client = new pg.Client(conString);
    var zones = [];


    var sqlQuery = 'SELECT ST_AsGeoJSON(the_geom) AS geometry FROM ' + tableName;

    client.connect(function(err) {
        if (err) {
            return console.error('Could not connect to PostgreSQL database.', err);
        }
        client.query(sqlQuery, function(err, result) {
            if (err) {
                return console.error('Error running query', err);
            }
            var rows = result.rows;
            for (var rowIndex = 0; rowIndex < rows.length; ++rowIndex) {
                var zone = rows[rowIndex].geometry;
                var jsonizedZone = JSON.parse(zone);
                zones.push(jsonizedZone);
                // console.log(jsonizedZone);
            }
            console.log("Successfull retrieved " + zones.length + " objects.")
            client.end();

            response.writeHead(200, {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
            });
            response.write(JSON.stringify(zones));
            response.end();
        });
    });
}

var server = app.listen(3000, function() {
    console.log('Listening on port %d', server.address().port);
});
