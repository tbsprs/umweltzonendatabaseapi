# Umweltzonen PostgreSQL/PostGIS API

A Node.js based HTTP API layer to access data
stored in a PostgreSQL/PostGIS database.


## Assumptions

It is assumed that PostgreSQL is running on the same machine. Further
the user has to import the Umweltzonen data. Please make sure that the
table names match what is configured in the application (see *app.js*).


## How to get Umweltzonen data into PostgreSQL?

1. Run the query in *commands/overpass-query.txt*
on [Overpass-Turbo][overpass-turbo] and export the result as a GeoJSON file.

2. Import the GeoJSON file into PostgreSQL with the following command:

    ```
    $ ogr2ogr -f "PostgreSQL" PG:"host=localhost user=USER port=5432 dbname=DATABASE password=PASSWORD" germany-umweltzonen-20141201.geojson -lco GEOMETRY_NAME=the_geom -nln "test.umweltzonen_status_20141201"
    ```

3. Run the following SQL command to remove `nodes` from the table
since we are just interested in `ways` and `relations`:

    ```
    DELETE FROM test.umweltzonen_status_20141201 WHERE "@id" LIKE '%node%';
    ```



## Usage

First load the environment via:

```bash
$ nvm install .
```

Then install all NPM dependencies by running this command:

```bash
$ npm install
```

Before you can run the server you need to specify
the connection parameters in `app.js`:

```js
var conString = "postgres://<USER>:<PASSWORD>@localhost/<DATABASE>";
```

You might also want to customize the SQL query to match your
database name and tables.

To run the server execute the following command:

```bash
$ node app.js
```

The server can be access via `http://localhost:3000`.


## API end points

There are different API end points to be access through the following paths:

* `http://localhost:3000/before` // Returns Umweltzonen changed before the annoucement of the week task
* `http://localhost:3000/changed` // Returns Umweltzonen changed since the annoucement of the week task



[overpass-turbo]: http://overpass-turbo.eu
